import { Component } from "react";
import likeImg from "../../../assets/images/1196px-Facebook_like_thumb.png";

class OutputMessage extends Component {
    render() {
        return (
            <div>
                <div className="row mt-3">
                    <div className="col-12">
                        <p>Thông điệp ở đây: </p>
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col-12">
                        <img src={likeImg} width="50px" className="img-thumbnail" />
                    </div>
                </div>
            </div>
        )
    }
}

export default OutputMessage;