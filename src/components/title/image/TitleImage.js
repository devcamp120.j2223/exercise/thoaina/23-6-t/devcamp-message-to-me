import { Component } from "react";
import image from "../../../assets/images/b68a5d6aac9d02151f721e2427c7fe79.jpg"

class TitleImage extends Component {
    render() {
        return (
            <div className="row mt-3">
                <div className="col-12">
                    <img src={image} width="500px" className="img-thumbnail" />
                </div>
            </div>
        )
    }
}

export default TitleImage;